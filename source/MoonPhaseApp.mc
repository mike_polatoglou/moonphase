using Toybox.Application as App;

class MoonPhaseApp extends App.AppBase {

	hidden var mModel;
    hidden var mView;

    function initialize() {
        AppBase.initialize();
    }

    //! onStart() is called on application start up
    function onStart() {
    	mView = new MoonPhaseView();
    }

    //! onStop() is called when your application is exiting
    function onStop() {
    }

    //! Return the initial view of your application here
    function getInitialView() {
        return [ mView ];
    }

}